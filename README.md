### A propos
Ce repo sera utilisé pour partager les bouts de code qu'on verra dans le TP et aussi les solutions disponibles en formats txt .

### Haskell

Pour commencer, il est nécessaire d'installer un Haskell compiler, je vous suggère d'utiliser GHC que vous pouvez trouver ici: [Haskell Platform](https://www.haskell.org/platform/)

Vous pouvez trouver un tutoriel complet sur [Haskell ici](http://learnyouahaskell.com/chapters).

Pour ceux qui ont envie d'expérimenter d'eux-mêmes trouver [ici un cheatSheet](https://cheatsheet.codeslower.com/CheatSheet.pdf)
