Exercice sur la portée:

{x -> 5; y -> 6; ...}(exp) dénote l'évaluation de exp dans l'environnement où x vaut 5, y vaut 6, etc.
On peut dessiner l'environnement comme une pile. Les fonctions dans la portée statiques doivent avoir leur propre pile de stocké avec le symbol.


Soit le code suivant :

let z = 1
    x = 2
    f1 y = z + x + y
    f2 x = f1 (x + 1)
    f3 z = f2 (z + 2)
in
  f3 5

Évaluation avec portée dynamique

{f3 -> \z = f2 (z + 2) (f3 5); f2 -> \x = f1 (x + 1); f1 -> \y = z + x + y; x -> 2; z -> 1} (f3 5)
{...} ((\z = f2 (z + 2)) 5)
{z -> 5; ...} (f2 (z + 2))
{z -> 5; ...} (f2 (5 + 2))
{z -> 5; ...} (f2 7)
{z -> 5; ...} ((\x = f1 (x + 1)) 7)
{x -> 7; z -> 5; ...} (f1 (x + 1))
{x -> 7; z -> 5; ...} (f1 (7 + 1))
{x -> 7; z -> 5; ...} (f1 8)
{x -> 7; z -> 5; ...} ((\y = z + x + y) 8)
{y -> 8; x -> 7; z -> 5; ...} (z + x + y) -- Ici x fait référence au x le plus récent dans l'environnement
                                          -- Même chose pour z
{y -> 8; x -> 7; z -> 5; ...} (5 + 7 + 8)
{y -> 8; x -> 7; z -> 5; ...} 20


Évaluation avec portée statique
let z = 1
    x = 2
    f1 y = z + x + y
    f2 x = f1 (x + 1)
    f3 z = f2 (z + 2)
in
  f3 5

Même chose mais on stock un petit environnement pour les variables libres

---------------------------------------------------

Grammaire si et seulement si:

<S> ::= <X>
    | 'if' <E> 'then' <S>
    | 'if' <E> 'then' <S> 'else' <S>


Exemple d'ambiguïté :

if E1 then if E2 then E3 else E4

 - if E1 then (if E2 then E3 else E4)
   OU
 - if E1 then (if E2 then E3) else E4


Grammaire pour lever l'ambiguïté.
- Arbitrairement décider que les else se réfèrent aux if les plus proches

<S> ::= <X>
    | 'if' <E> 'then' <S>
    | 'if' <E> 'then' <SElse> 'else' <S>

<SElse> ::=
    | 'if' <E> 'then' <SElse> 'else' <SElse>
