-- datatype:
data Exp e = Enum Int | Eplus (Exp e) (Exp e)
  deriving (Show, Eq)

--definition de la fonction eval
eval:: Exp e -> Int
eval (Enum a ) = a
eval ( Eplus (c) (b) ) = eval (b) +  eval (c)

--exemple
foo = Eplus (Enum 1) (Enum 1)
bar = Eplus (foo) (Enum 1)
